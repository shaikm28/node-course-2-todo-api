require('./config/config');
const express = require('express');
const bodyParser = require('body-parser');
const _ = require('lodash');


const {ObjectID} = require('mongodb');

var {mongoose} = require('./db/mongoose');
const {authenticate} = require('./middleware/authenticate');

var {Todo} = require('./model/todo');
var {User} = require('./model/user');

const port = process.env.PORT;

var app = express();

app.use(bodyParser.json());

app.post('/todos', (req, res) => {
    var todo = new Todo({
        text: req.body.text
    });

    todo.save().then((doc) => {
        console.log('Saved todo: ', JSON.stringify(doc, undefined, 2));
        res.send(doc);
    }, (e) => {
        console.log('Error while saving: ', e);
        res.status(400).send(e);

    });
});

app.get('/todos', (req, res) => {
    Todo.find().then((docs) => {
        res.send({ docs });
    }, (e) => {
        res.status(400).send(e);
    });
});

app.get('/todos/:id', (req, res) => {
    var id = req.params.id;

    if (!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    Todo.findById(id).then((doc) => {
        if (doc) {
            res.status(200).send({ doc });
        } else {
            res.status(404).send();
        }
    }).catch((e) => {
        res.status(400).send();
    });
});

app.delete('/todos/:id', (req, res) => {

    var id = req.params.id;
    if (!ObjectID.isValid(id)) {
        return res.status(404).send();
    }
    Todo.findByIdAndRemove(id).then((todo) => {
        if (todo) {
            return res.status(200).send({ todo });
        }
        res.status(404).send();
    }).catch(() => {
        res.status(400).send();
    });
});

app.patch('/todos/:id', (req, res) => {

    var id = req.params.id;
    var body = _.pick(req.body, ['text', 'completed']);

    if (!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    if (_.isBoolean(body.completed) && body.completed) {
        body.completedAt = new Date().getTime();
    } else {
        body.completed = false;
        body.completedAt = null;
    }

    Todo.findByIdAndUpdate(id, {
        $set: body
    }, {
            new: true
        }).then((todo) => {
            if (!todo) {
                return res.status(404).send();
            }
            return res.status(200).send({ todo });
        }).catch(e => res.status(400).send());

});

app.post('/users', (req, res) => {
    var body = _.pick(req.body, ['email', 'password']);
    var user = new User(body);
    user.save().then(() => {
        return user.generateAuthToken();
    })
        .then((token) => {
            console.log('Saved user :', user);
            return res.header('x-auth', token).send({ user });
        }).catch((e) => {
            console.log('Unable to save user: ', e);
            return res.status(400).send();
        });
});

app.get('/users/', (req, res) => {
    User.find().then((users) => {
        res.status(200).send({ users });
    }).catch((e) => {
        console.log('Unable to get users: ', e);
        res.status(400).send();
    });
});

// app.get('/users/:id', (req, res) => {

//     var id = _.pick(req.params, ['id']).id;

//     if (!ObjectID.isValid(id)) {
//         console.log(`ID ${id} is not valid`);
//         return res.status(404).send();
//     }

//     User.findById(id).then((user) => {
//         if (user) {
//             return res.status(200).send({ user });
//         }
//         return res.status(404).send();
//     }).catch((e) => {
//         return res.status(400).send();
//     });
// });

app.patch('/users/:id', (req, res) => {

    var body = _.pick(req.body, ['email', 'password']);
    console.log(`body is ${body}`);

    var id = _.pick(req.params, ['id']).id;
    if (!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    User.findByIdAndUpdate(id, {
        $set: body
    }, {
            new: true
        }).then((user) => {
            if (!user) {
                return res.status(404).send();
            }
            res.status(200).send({ user });
        }).catch((e) => {
            console.log('Unable to update user: ', e);
            return res.status(400).send();
        });

});

app.delete('/users/:id', (req, res) => {
    var id = _.pick(req.params, ['id']).id;
    if (!ObjectID.isValid(id)) {
        return res.status(404).send();
    }
    User.findByIdAndRemove(id).then((user) => {
        if (!user) {
            console.log('Unable to delete user');
            return res.status(404).send();
        }
        return res.status(200).send({ user });
    }).catch((e) => {
        console.log('Unable to delete user: ', e);
        return res.status(400).send();
    });

});

app.get('/users/me', authenticate, (req, res) => {
    res.send(req.user);
});

app.listen(port, () => {
    console.log(`Server is up on port ${port} !`);
});

module.exports = { app };