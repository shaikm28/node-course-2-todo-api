const expect = require('expect');
const request = require('supertest');
const {ObjectID} = require('mongodb');

const {app} = require('./../server');
const {Todo} = require('./../model/todo');

const seedTodos = [{
    _id: new ObjectID(),
    text: '1st todo'
}, {
    _id: new ObjectID(),
    text: '2nd todo',
    completed: true
}, {
    _id: new ObjectID(),
    text: '3rd todo'
}];

beforeEach((done) => {
    Todo.remove({}).then(() => {
        return Todo.insertMany(seedTodos);
    }).then(() => done());
});

describe('POST /todos', () => {
    it('should create a new todo', (done) => {

        var text = 'sample todo';
        request(app)
            .post('/todos')
            .send({ text })
            .expect(200)
            .expect((res) => {
                expect(res.body.text).toBe(text);
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                Todo.find({ text }).then((todos) => {
                    expect(todos.length).toBe(1);
                    expect(todos[0].text).toBe(text);
                    done();
                }).catch((e) => done(e));
            });
    });

    it('should not create a todo', (done) => {
        request(app)
            .post('/todos')
            .send(' ')
            .expect(400)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                Todo.find().then((todos) => {
                    // the previous length of 3 should remain unchanged
                    expect(todos.length).toBe(3);
                    done();
                });
            });
    });
});

describe('GET /todos', () => {

    it('should get the todos', (done) => {
        request(app)
            .get('/todos')
            .expect(200)
            .expect((res) => {
                expect(res.bodytodos.length).tobe(3);
            })
            .end(done());
    });

});

describe('GET /todos/:id', () => {
    it('should get an object by id', (done) => {
        request(app)
            .get(`/todos/${seedTodos[0]._id.toHexString()}`)
            .expect(200)
            .expect((res) => {
                expect(res.body.doc._id).toBe(seedTodos[0]._id.toHexString());
            })
            .end(done);
    });

    it('should return 404 for an invalid id', (done) => {
        request(app)
            .get('/todos/123')
            .expect(404)
            .end(done);
    });

    it('should return 404 for an id not found in db', (done) => {
        request(app)
            .get('/todos/68832370af135fc4c4471377')
            .expect(404)
            .end(done);
    });
});

describe('DELETE /todos/:id', () => {
    it('should delete an object by id', (done) => {
        request(app)
            .delete(`/todos/${seedTodos[1]._id.toHexString()}`)
            .expect(200)
            .expect((res) => {
                expect(res.body.todo.text).toBe(seedTodos[1].text);
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                Todo.findById(seedTodos[1]._id.toHexString()).then((todo) => {
                    expect(todo).toNotExist();
                    done();
                }).catch((e) => done(e));
            })
    });


    it('should return 404 for an invalid id', (done) => {
        request(app)
            .delete('/todos/123')
            .expect(404)
            .end((err, res) => {
                console.log('error : ', err);
                done(err);
            });
    });

    it('should return 404 for an id not found in db', (done) => {
        request(app)
            .delete('/todos/68832370af135fc4c4471377')
            .expect(404)
            .end(done);
    });
});

describe('PATCH /todos/:id', () => {
    it('should update an object by id', (done) => {
        request(app)
            .patch(`/todos/${seedTodos[0]._id.toHexString()}`)
            .send({
                'completed': true,
                'text': 'Shaik M3'
            })
            .expect(200)
            .expect((res) => {
               expect(res.body.todo.text).toBe('Shaik M3');
                expect(res.body.todo.completed).toBe(true);
                expect(res.body.todo.completedAt).toBeA('number');
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it ('should clear completedAt when todo is not completed', (done) => {
        request(app)
            .patch(`/todos/${seedTodos[1]._id.toHexString()}`)
            .send({
                'completed': false,
                'text': 'new shaik m'
            })
            .expect(200)
            .expect((res) => {
                expect(res.body.todo.text).toBe('new shaik m');
                expect(res.body.todo.completed).toBe(false);
                expect(res.body.todo.completedAt).toNotExist();
            })
            .end((err, res) => {
                done();
            });
    });

    it('should return 404 for an invalid id', (done) => {
        request(app)
            .patch('/todos/123')
            .expect(404)
            .end((err, res) => {
                done(err);
            });
    });

    it('should return 404 for an id not found in db', (done) => {
        request(app)
            .patch('/todos/68832370af135fc4c4471377')
            .expect(404)
            .end(done);
    });

});
