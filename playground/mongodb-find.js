const MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {

    if (err) {
        return console.log('MongoDB Connection error');
    }
    console.log('MongoDB Connection Success!');

    // db.collection('Todos').insertOne({
    //     text: 'Sample text',
    //     completed: false
    // }, (err, result) => {
    //     if (err) {
    //         return console.log('Insertion failed');
    //     }
    //     console.log(JSON.stringify(result.ops, undefined, 2));
    // });

// db.collection('Users').insertOne({
//     _id: 12345,
//     name: 'Shaik M',
//     age: 30,
//     location: 'Canada'
// }, (err, res) => {
//     if (err) {
//         return console.log('Error while inserting.. ', err);
//     }
//     console.log(JSON.stringify(res.ops, undefined, 2));
//     console.log('Insertion Success');
// });

db.collection('Users').find({
    name: 'Shaik M'
}).toArray().then((docs) => {
    console.log(JSON.stringify(docs, undefined, 2));
}, (err) => {
    console.log('Error', err);
});

db.close();
});