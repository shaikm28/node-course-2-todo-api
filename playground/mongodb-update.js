const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {

    if (err) {
        return console.log('MongoDB Connection error');
    }
    console.log('MongoDB Connection Success!');

    db.collection('Users').findOneAndUpdate({
        _id: new ObjectID("5880d2809ba921844f51fae6")
    },{
        $set: {
            name: 'Shaik Muhammad'
        }, 
        $inc: {
            age: 1
        }
    }, {
        returnOriginal: false
    }
    ).then((res) => {
        console.log(res);
    })


    // db.close();
});