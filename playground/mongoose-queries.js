const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {User} = require('./../server/model/user');

var validId = '5881bd6a4c62f2a3997a35e3';

var nonExistentId = '6881bd6a4c62f2a3997a35e3';

var invalidId = '6881bd6a4c62f2a3997a35e31234';

if (!ObjectID.isValid(invalidId)) {
    console.log(`Object ID ${invalidId} is not valid`);
}

console.log('find valid id');
User.findById(validId).then((doc) => {
    console.log('inside valid id');
    console.log(doc);
}, (e) => { 
    console.log(e);
});

console.log('find nonExistentId id');
User.findById(nonExistentId).then((doc) => {
    console.log('inside nonExistentId id');
    console.log(doc);
}, (e) => {
    console.log(e);
});

console.log('find invalidId id');
User.findById(invalidId).then((doc) => {
    console.log('inside invalidId id');
    console.log(doc);
}, (e) => {
    console.log(e);
});


