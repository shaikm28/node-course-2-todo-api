const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {

    if (err) {
        return console.log('MongoDB Connection error');
    }
    console.log('MongoDB Connection Success!');

    // db.collection('Users').deleteMany({
    //     name: 'Shaik M'
    // }).then((res) => {
    //     console.log(res);
    // });

    // db.collection('Users').deleteOne({
    //     _id: new ObjectID('5880a8e19ba921844f51fae6')
    // }).then((res) => {
    //     console.log(res);
    // });

    db.collection('Users').findOneAndDelete({
        name: 'Shaik Muhammad Abdul Razak'
    }).then((res) => {
        console.log(res);
    });

    // db.close();
});